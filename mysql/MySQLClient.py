import MySQLdb

db = MySQLdb.connect(host='192.168.1.156', port=3306, user='root', passwd='test_root_ds12345!@#$%1q4$GF3ds#0', db='test', charset='utf8')

cursor = db.cursor()

# 使用 execute()  方法执行 SQL 查询
cursor.execute("SELECT * FROM novel")

# 使用 fetchall() 方法获取s所有数据.
data = cursor.fetchall()
print(data)

cursor.close()
db.close()

