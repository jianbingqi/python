#!/usr/bin/python3

import pymysql

# 打开数据库连接
db = pymysql.connect("192.168.1.156", "root", "test_root_ds12345!@#$%1q4$GF3ds#0", "test")

# 使用 cursor() 方法创建一个游标对象 cursor
cursor = db.cursor()

# 使用 execute()  方法执行 SQL 查询
cursor.execute("SELECT * FROM novel WHERE id=1")

# 使用 fetchall() 方法获取s所有数据.
data = cursor.fetchall()

print(data)

# 关闭数据库连接
db.close()

